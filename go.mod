module gitlab.com/lizt_mvalley/short_url

go 1.13

require (
	github.com/golang/protobuf v1.3.5
	github.com/jinzhu/gorm v1.9.12
	github.com/twitchtv/twirp v5.10.1+incompatible
	go.uber.org/zap v1.14.1
	google.golang.org/grpc v1.28.0
)
