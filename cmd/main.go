package main

import (
	_ "gitlab.com/lizt_mvalley/short_url/internal/log"
	"gitlab.com/lizt_mvalley/short_url/internal/storage"
	pro "gitlab.com/lizt_mvalley/short_url/proto"
	"gitlab.com/lizt_mvalley/short_url/rpc"
	"net/http"
)

func init() {
	// TODO: configs 序列化操作
}

func main() {
	storage.Init()
	server := &rpc.ShortServer{}
	twripHandler := pro.NewShortSeverServer(server, nil)

	http.ListenAndServe(":8800", twripHandler)
}
