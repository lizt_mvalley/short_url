package rpc

import (
	"context"
	"gitlab.com/lizt_mvalley/short_url/internal/shorturl"
	"gitlab.com/lizt_mvalley/short_url/internal/storage"
	"log"
)
import pro "gitlab.com/lizt_mvalley/short_url/proto"

type ShortServer struct {
}

func (*ShortServer) StoL(ctx context.Context, url *pro.Request) (*pro.URL, error) {
	long, err := shorturl.ToLongUrl(url.Url)
	if err != nil {
		log.Println("StoL：", url, " err:", err)
	}

	return long.ToProto(), err
}

func (*ShortServer) LtoS(ctx context.Context, url *pro.Request) (*pro.URL, error) {
	short, err := shorturl.ToShortUrl(url.Url)
	if err != nil {
		log.Println("LtoS：", url, " err:", err)
	}
	storage.UrlChan <- short
	return short.ToProto(), nil
}
