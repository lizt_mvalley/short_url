package model

import (
	pro "gitlab.com/lizt_mvalley/short_url/proto"
	"time"
)

type Url struct {
	ID        int
	LongUrl   string
	ShortUrl  string
	Message   string
	Survival  int64
	Timestamp int64
	Expire    bool
}

func (v *Url) IsExpire() bool {
	if time.Now().Unix()-v.Timestamp > v.Survival {
		v.Expire = true
		return true
	}
	return false
}

func FromProto(url *pro.URL) *Url {
	return &Url{
		LongUrl:  url.Longurl,
		ShortUrl: url.Shorturl,
		Message:  url.Message,
	}
}

func (url *Url) ToProto() *pro.URL {
	return &pro.URL{
		Shorturl: url.ShortUrl,
		Longurl:  url.LongUrl,
		Message:  url.Message,
	}
}
