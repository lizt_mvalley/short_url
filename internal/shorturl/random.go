package shorturl

import (
	"bytes"
	"math/rand"
	"time"
)

func commonMap()string{
	return "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
}

// 由于为加密算法（MD5），即可能存在碰撞的情况
// 乱序以减少碰撞
// TODO：效率有点差，暂时并未使用
func randomMap()string{
	var charMap = commonMap()
	rand.Seed(int64(time.Now().Second()))
	var l = 61
	var buffer bytes.Buffer
	for l > 0{
		randIndex := rand.Intn(l)
		buffer.WriteByte(charMap[randIndex])
		charMap = charMap[0:randIndex]+charMap[randIndex+1:]
		l--
	}
	// 上述循环结束后，charMap内还剩一元素，也需要写入buffer
	buffer.WriteByte(charMap[0])
	return buffer.String()
}
