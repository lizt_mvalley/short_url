// TODO：性能测试
package shorturl

import (
	"log"
	"testing"
)

// 暂时未考虑性能测试，只完成功能测试

// 用以检测生成的字符串元素是否和原有字符串相同
func check(s string) bool {
	if len(s) != 62 {
		return false
	}

	// 将字符串做成字典，用于后面判断
	var s_map = make(map[byte]bool)
	for i, _ := range s {
		s_map[s[i]] = true
	}

	// 获取原字符串（使用RandomeMap同一函数）
	var s_check string = commonMap()
	for i, _ := range s_check {
		if !s_map[s_check[i]] {
			return false
		}
	}

	return true
}

func TestRandomMap(t *testing.T) {
	s := randomMap()
	if !check(s){
		log.Fatalf("%s\n%s\n%s","新生城字符串与原字符串元素有出入",commonMap(),s)
	}
}
