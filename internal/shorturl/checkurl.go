package shorturl

import (
	"errors"
	"gitlab.com/lizt_mvalley/short_url/internal/model"
	"gitlab.com/lizt_mvalley/short_url/internal/storage"
	"gitlab.com/lizt_mvalley/short_url/internal/urlerror"
)

func checkShortUrl(url *model.Url) (*model.Url, error) {
	if storage.UrlMap.ExistsInverse(url.ShortUrl) {
		u, _ := storage.UrlMap.GetInverse(url.ShortUrl)
		if !u.Expire {
			return u, errors.New("此网址已有对应的短网址")
		}

		storage.ExpireChan <- u
		if err := storage.UrlMap.DeleteInverse(url.ShortUrl); err != nil {
			return urlerror.ShortIntervalError(url.ShortUrl), errors.New("删除已有的过期短网址失败")
		}
	}

	return nil, nil
}

func checkLongUrl(url *model.Url) (*model.Url, error) {
	if storage.UrlMap.Exists(url.LongUrl) {
		u, _ := storage.UrlMap.Get(url.LongUrl)
		if !u.Expire {
			return u, errors.New("此网址已有对应的短网址")
		}

		storage.ExpireChan <- u
		if err := storage.UrlMap.Delete(url.LongUrl); err != nil {
			return urlerror.LongIntervalError(url.LongUrl), errors.New("删除已有的过期短网址失败")
		}
	}

	return nil, nil
}
