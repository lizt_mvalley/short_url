package shorturl

import (
	"crypto/md5"
	"errors"
	"fmt"
	"gitlab.com/lizt_mvalley/short_url/internal/model"
	"strconv"
)

var Domain = "http://mvalley.io/"

// 生成6字符短key
// 根据url生成32字符的签名，将其分成4段，每段8位字符
// 循环处理4段8位字符，将每段转换成16进制与0x3FFFFFFF进行逻辑与操作，得到30位的无符号数
// 将30位数分成6段，依次得到6个0-61的数字索引查字符集表获得6位字符串
func generator6(url *model.Url, charset, hexMd5 string, sectionNum int) (*model.Url, error) {
	for i := 0; i < sectionNum; i++ {
		sectionHex := hexMd5[i*8 : 8+i*8]
		bits, _ := strconv.ParseUint(sectionHex, 16, 32)
		bits = bits & 0x3FFFFFFF

		keyword := ""
		for j := 0; j < 6; j++ {
			idx := bits & 0x3D
			keyword = keyword + string(charset[idx])
			bits = bits >> 5
		}
		if v, err := checkShortUrl(&model.Url{ShortUrl: keyword}); err != nil {
			return v, err
		}
		url.ShortUrl = keyword
		url.Message = "转换成功"
		return url, nil
	}
	return nil, errors.New("操作完成但转换失败")
}

// 生成8字符短key
func generator8(url *model.Url, charset, hexMd5 string, sectionNum int) (*model.Url, error) {
	for i := 0; i < sectionNum; i++ {
		sectionHex := hexMd5[i*8 : i*8+8]
		bits, _ := strconv.ParseUint(sectionHex, 16, 32)
		bits = bits & 0xFFFFFFFF

		keyword := ""
		for j := 0; j < 8; j++ {
			idx := bits & 0x3D
			keyword = keyword + string(charset[idx])
			bits = bits >> 4
		}
		if v, err := checkShortUrl(&model.Url{ShortUrl: keyword}); err != nil {
			return v, err
		}
		url.ShortUrl = keyword
		url.Message = "转换成功"
		return url, nil
	}

	return nil,errors.New("操作完成但转换失败")
}

// 生成6-8字符的短链接
// 起初生成6位的短链接，当四组6位短链接都重复时，再生成8位的短链接
func ToShortUrl(u string) (*model.Url, error) {
	if u == "" {
		return nil, errors.New("未输入正确的url")
	}

	var url = &model.Url{LongUrl: u}
	v, err := checkLongUrl(url)
	if err != nil {
		return v, err
	}
	charset := commonMap()
	hexMd5 := fmt.Sprintf("%x", md5.Sum([]byte(url.LongUrl)))
	sections := len(hexMd5) / 8

	out, err := generator6(url, charset, hexMd5, sections)
	if err != nil {
		out, err = generator8(url, charset, hexMd5, sections)
		if err != nil {
			return url, err
		}
	}

	return out, nil
}
