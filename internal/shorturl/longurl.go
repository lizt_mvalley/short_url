package shorturl

import (
	"errors"
	"gitlab.com/lizt_mvalley/short_url/internal/model"
	"gitlab.com/lizt_mvalley/short_url/internal/storage"
	"gitlab.com/lizt_mvalley/short_url/internal/urlerror"
)

func ToLongUrl(u string) (*model.Url, error) {
	url, ok := storage.UrlMap.GetInverse(u)
	if ok {
		return url, nil
	}

	return urlerror.NoLongUrlError(u), errors.New("没有对应的长网址")
}
