package storage

import (
	"gitlab.com/lizt_mvalley/short_url/internal/model"
	"log"
	"testing"
	"time"
)

var url = &model.Url{
	ID:        4,
	LongUrl:   "www.facebook.com",
	ShortUrl:  "000333",
	Message:   "success",
	Survival:  1000,
	Timestamp: time.Now().Unix(),
	Expire:    true,
}

// gorm_init
func TestInit(t *testing.T) {}

func TestAllIn(t *testing.T) {
	// TODO
}

func TestDB_GetAllURL(t *testing.T) {
	urls, err := DBClient.GetAllURL()
	if err != nil {
		log.Fatalln(err)
	}
	for _, val := range urls {
		log.Println(val)
	}
}

func TestDB_MappingURL(t *testing.T) {
	urls, err := DBClient.MappingURL()
	if err != nil {
		log.Fatalln(err)
	}
	for _, val := range urls {
		log.Println(val)
	}
}

func TestDB_Insert(t *testing.T) {
	if err := DBClient.Insert(url); err != nil {
		log.Fatalln(err)
	}
}

func TestDB_Delete(t *testing.T) {
	if err := DBClient.Delete(url); err != nil {
		log.Fatalln(err)
	}
}
