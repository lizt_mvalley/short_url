package storage

import (
	"errors"
	"gitlab.com/lizt_mvalley/short_url/internal/model"
	"sync"
)

var UrlMap *UrlBiMap

func init() {
	UrlMap = NewUrlBiMap()
}

// 由于主要逻辑为转换长网址到短网址
// 故实现过程中将Bimap的key定义为长网址，val定义为短网址
type UrlBiMap struct {
	s         sync.RWMutex
	immutable bool
	forward   map[string]*model.Url
	inverse   map[string]*model.Url
}

func NewUrlBiMap() *UrlBiMap {
	return &UrlBiMap{forward: make(map[string]*model.Url), inverse: make(map[string]*model.Url), immutable: false}
}

// 频繁的R锁和全局锁
// TODO:减少锁的竞争
func (b *UrlBiMap) BatchInsert(v []*model.Url) error {
	for i, _ := range v {
		if b.ExistsInverse(v[i].ShortUrl) {
			// insert唯一错误为将数据改为不可写，故此可以直接返回
			if err := b.Insert(v[i]); err != nil {
				return err
			}
		}
	}
	return nil
}

func (b *UrlBiMap) Insert(v *model.Url) error {
	b.s.RLock()
	if b.immutable {
		return errors.New("不能修改状态为'已锁定‘的map")
	}
	b.s.RUnlock()

	b.s.Lock()
	defer b.s.Unlock()
	b.forward[v.LongUrl] = v
	b.inverse[v.ShortUrl] = v
	return nil
}

func (b *UrlBiMap) Exists(k string) bool {
	b.s.RLock()
	v, ok := b.forward[k]
	b.s.RUnlock()
	if ok && v.IsExpire() {
		b.Delete(v.LongUrl)
		return false
	}
	return ok
}

func (b *UrlBiMap) ExistsInverse(k string) bool {
	b.s.RLock()
	v, ok := b.inverse[k]
	b.s.RUnlock()
	if ok && v.IsExpire() {
		b.DeleteInverse(v.ShortUrl)
		return false
	}
	return ok
}

func (b *UrlBiMap) Get(k string) (*model.Url, bool) {
	if !b.Exists(k) {
		return nil, false
	}
	b.s.RLock()
	defer b.s.RUnlock()
	return b.forward[k], true

}

func (b *UrlBiMap) GetInverse(v string) (*model.Url, bool) {
	if !b.ExistsInverse(v) {
		return nil, false
	}
	b.s.RLock()
	defer b.s.RUnlock()
	return b.inverse[v], true

}

func (b *UrlBiMap) Delete(k string) error {
	b.s.RLock()
	if b.immutable {
		return errors.New("不能修改状态为'已锁定‘的map")
	}
	b.s.RUnlock()

	if !b.Exists(k) {
		return nil
	}

	val, _ := b.Get(k)
	b.s.Lock()
	defer b.s.Unlock()
	delete(b.forward, k)
	delete(b.inverse, val.ShortUrl)
	return nil
}

func (b *UrlBiMap) DeleteInverse(v string) error {
	b.s.RLock()
	if b.immutable {
		errors.New("不能修改状态为'已锁定‘的map")
	}
	b.s.RUnlock()

	if !b.ExistsInverse(v) {
		return nil
	}

	key, _ := b.GetInverse(v)
	b.s.Lock()
	defer b.s.Unlock()
	delete(b.inverse, v)
	delete(b.forward, key.LongUrl)
	return nil
}

func (b *UrlBiMap) Size() int {
	b.s.RLock()
	defer b.s.RUnlock()
	return len(b.forward)
}

func (b *UrlBiMap) MakeImmutable() {
	b.s.Lock()
	defer b.s.Unlock()
	b.immutable = true
}

func (b *UrlBiMap) GetInverseMap() map[string]*model.Url {
	return b.inverse
}

func (b *UrlBiMap) GetForwardMap() map[string]*model.Url {
	return b.forward
}

func (b *UrlBiMap) Lock() {
	b.s.Lock()
}

func (b *UrlBiMap) Unlock() {
	b.s.Unlock()
}
