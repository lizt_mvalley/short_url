package storage

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"gitlab.com/lizt_mvalley/short_url/internal/model"
	"log"
)

var DBClient DB

// 用以异步存入mysql（未实现工作池操作）
// TODO：工作池化
var UrlChan = make(chan *model.Url)
var ExpireChan = make(chan *model.Url)

type DB struct {
	engine *gorm.DB
}

type MockDB interface {
	GetAllURL() ([]*model.Url, error)
	MappingURL() ([]*model.Url, error)
	Insert(*model.Url) error
	Delete(*model.Url) error
}

func init() {
	var err error
	DBClient.engine, err = gorm.Open("mysql", "root:123456@(localhost:33060)/mvalley?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		log.Fatal(err)
	}

	if !DBClient.engine.HasTable(&model.Url{}) {
		if err := DBClient.engine.
			Set("gorm:table_option", nil).CreateTable(&model.Url{}).Error; err != nil {
			panic(err)
		}
	}

	go UrlChanWorker()
	go ExpireChanWorker()
}

func UrlChanWorker() {
	for {
		url := <-UrlChan
		DBClient.Insert(url)
	}
}

func ExpireChanWorker() {
	for {
		url := <-ExpireChan
		DBClient.ExpireUpdate(url)
	}
}

func (*DB) GetAllURL() ([]*model.Url, error) {
	var out []*model.Url
	if err := DBClient.engine.Find(&out).Error; err != nil {
		return nil, err
	}
	return out, nil
}

func (*DB) MappingURL() ([]*model.Url, error) {
	var out []*model.Url
	err := DBClient.engine.Where("expire = ?", false).Find(&out).Error
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (*DB) Insert(url *model.Url) error {
	if err := DBClient.engine.Create(url).Error; err != nil {
		return err
	}
	return nil
}

func (*DB) Delete(url *model.Url) error {
	if err := DBClient.engine.Where("id = ?", url.ID).Delete(&model.Url{}).Error; err != nil {
		return err
	}
	return nil
}

func (*DB) ExpireUpdate(url *model.Url) error {
	if err := DBClient.engine.Model(&model.Url{}).
		Where("id = ?", url.ID).Update("expire", true).Error; err != nil {
		return err
	}
	return nil
}
