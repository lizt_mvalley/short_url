package storage

import (
	"log"
	"testing"
)

// BiMap 集成测试
func TestBiMap(t *testing.T) {
	biMap := NewBiMap()
	biMap.Insert("key", "value")
	val, ok := biMap.GetInverse("value") // 值应该是"key", ok应该是true
	if !ok || val != "key"{
		log.Fatalln("BiMap 构建失败")
	}
	biMap.Delete("key")
	biMap.Size() // == 0

	biMap2 := NewBiMap()
	biMap2.Insert("key", 1) // 使用不同类型
	val1, ok := biMap2.Get("key") // 返回 int 1
	if !ok || val1 != int(1){
		log.Fatalln("BiMap 构建失败")
	}
	val2, ok := biMap2.GetInverse(1) // 返回 "key"
	if !ok || val2 != "key"{
		log.Fatalln("BiMap 构建失败")
	}
}
