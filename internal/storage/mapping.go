package storage

import "log"

func Init() {
	urls, err := DBClient.MappingURL()
	if err != nil {
		log.Fatalln(err)
	}

	if err := UrlMap.BatchInsert(urls); err != nil {
		log.Fatalln(err)
	}
}
