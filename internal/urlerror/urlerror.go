package urlerror

import "gitlab.com/lizt_mvalley/short_url/internal/model"

func ShortIntervalError(url string) *model.Url {
	return &model.Url{
		LongUrl:  "",
		ShortUrl: url,
		Message:  "转换失败,内部错误",
	}
}

func LongIntervalError(url string) *model.Url {
	return &model.Url{
		LongUrl:  url,
		ShortUrl: "",
		Message:  "转换失败,内部错误",
	}
}

func NoLongUrlError(url string) *model.Url {
	return &model.Url{
		LongUrl:  "",
		ShortUrl: url,
		Message:  "没有对应的长网址",
	}
}